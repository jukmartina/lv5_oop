﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;

namespace LV5_mmj
{
    public partial class Form1 : Form
    {
        TcpClient client;
        NetworkStream stream;
        String[] colors = { "red", "green", "blue", "yellow"};
        bool connectionUp = false;
        public Form1()
        {
            InitializeComponent();
            panel1.BackColor = Color.Red;
            tabPage1.Text = "Connect";
            tabPage2.Text = "Linija";
            tabPage3.Text = "Trokut";
            tabPage4.Text = "Pravokutnik";
            tabPage5.Text = "Poligon";
            tabPage6.Text = "Kruznica";
            tabPage7.Text = "Elipsa";
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void connectBtn_Click(object sender, EventArgs e)
        {
            
            client = new TcpClient(hostName.Text.ToString(), Int32.Parse(hostPort.Text.ToString()));
            stream = client.GetStream();
            panel1.BackColor = Color.Green;
            connectionUp = true;
        }

        private void disconnectBtn_Click(object sender, EventArgs e)
        {
            stream.Close();
            client.Close();
            panel1.BackColor = Color.Red;
            connectionUp= false;
        }

        private void linijaSend_Click(object sender, EventArgs e)
        {
            if(linijaBoja.Text.Length==0 || linija1x.Text.Length==0 || linija1y.Text.Length==0 || linija2x.Text.Length==0 || linija2y.Text.Length == 0)
            {
                MessageBox.Show("Some elements are missing! Make sure you enter all values.");
                return;
            }
            if (!connectionUp)
            {
                MessageBox.Show("Connect to the server and try again!");
                return;
            }
            string message = "Line " + linijaBoja.Text.ToString() + " " + linija1x.Text.ToString() + " " + linija1y.Text.ToString() + " " + linija2x.Text.ToString() + " " + linija2y.Text.ToString();
            int byteCount = Encoding.ASCII.GetByteCount(message); //broj bajtova u poruci
            byte[] sendData = new byte[byteCount]; //Bajt podaci koji se šalju
            sendData = Encoding.ASCII.GetBytes(message);
            stream.Write(sendData, 0, sendData.Length);
            linijaBoja.Text = "";
            linija1x.Text = "";
            linija1y.Text = "";
            linija2x.Text = "";
            linija2y.Text = "";
        }

        private void linijaRandom_Click(object sender, EventArgs e)
        {
            Random random = new Random();
            int index = random.Next(0, colors.Length);
            linijaBoja.Text = colors[index];
            linija1x.Text = random.Next(0, 1000).ToString();
            linija1y.Text = random.Next(0, 700).ToString();
            linija2x.Text = random.Next(0, 1000).ToString();
            linija2y.Text = random.Next(0, 700).ToString();
        }

        private void trokutRandom_Click(object sender, EventArgs e)
        {
            Random random = new Random();
            int index = random.Next(0, colors.Length);
            trokutBoja.Text = colors[index];
            trokut1x.Text = random.Next(0, 1000).ToString();
            trokut1y.Text = random.Next(0, 700).ToString();
            trokut2x.Text = random.Next(0, 1000).ToString();
            trokut2y.Text = random.Next(0, 700).ToString();
            trokut3x.Text = random.Next(0, 1000).ToString();
            trokut3y.Text = random.Next(0, 700).ToString();
        }

        private void trokutSend_Click(object sender, EventArgs e)
        {
            if (trokutBoja.Text.Length == 0 || trokut1x.Text.Length == 0 || trokut1y.Text.Length == 0 || trokut2x.Text.Length == 0 || trokut2y.Text.Length == 0 || trokut3x.Text.Length == 0 || trokut3y.Text.Length == 0)
            {
                MessageBox.Show("Some elements are missing! Make sure you enter all values.");
                return;
            }
            if (!connectionUp)
            {
                MessageBox.Show("Connect to the server and try again!");
                return;
            }
            string message = "Triangle " + trokutBoja.Text.ToString() + " " + trokut1x.Text.ToString() + " " + trokut1y.Text.ToString() + " " + trokut2x.Text.ToString() + " " + trokut2y.Text.ToString() + " " + trokut3x.Text.ToString() + " " + trokut3y.Text.ToString();
            int byteCount = Encoding.ASCII.GetByteCount(message); //broj bajtova u poruci
            byte[] sendData = new byte[byteCount]; //Bajt podaci koji se šalju
            sendData = Encoding.ASCII.GetBytes(message);
            stream.Write(sendData, 0, sendData.Length);
            trokutBoja.Text = "";
            trokut1x.Text = "";
            trokut1y.Text = "";
            trokut2x.Text = "";
            trokut2y.Text = "";
            trokut3x.Text = "";
            trokut3y.Text = "";
        }

        private void pravokutnikRandom_Click(object sender, EventArgs e)
        {
            Random random = new Random();
            int index = random.Next(0, colors.Length);
            pravokutnikBoja.Text = colors[index];
            int sirina = random.Next(0, 1000);
            int visina = random.Next(0, 700);
            pravokutnikSirina.Text = sirina.ToString();
            pravokutnikVisina.Text = visina.ToString();
            pravokutnik1x.Text = random.Next(0, 1000 - sirina).ToString();
            pravokutnik1y.Text = random.Next(0, 700 - visina).ToString();
        }

        private void poligonRandom_Click(object sender, EventArgs e)
        {
            Random random = new Random();
            int index = random.Next(0, colors.Length);
            poligonBoja.Text = colors[index];
            int numPoints = random.Next(3, 15);
            richTextBox1.Clear();
            for(int i = 0; i < numPoints; i++)
            {
                richTextBox1.AppendText(random.Next(0, 1000).ToString() + " " + random.Next(0, 700).ToString() + " ");
            }
        }

        private void kruznicaRandom_Click(object sender, EventArgs e)
        {
            Random random = new Random();
            int index = random.Next(0, colors.Length);
            kruznicaBoja.Text = colors[index];
            int radius = random.Next(0, 350);
            kruznicaR.Text = radius.ToString();
            kruznicaX.Text = random.Next(0 + radius, 1000 - radius).ToString();
            kruznicaY.Text = random.Next(0 + radius, 700 - radius).ToString();
        }

        private void elipsaRandom_Click(object sender, EventArgs e)
        {
            Random random = new Random();
            int index = random.Next(0, colors.Length);
            elipsaBoja.Text = colors[index];
            int radius1 = random.Next(0, 500);
            int radius2 = random.Next(0, 350);
            elipsaR1.Text = radius1.ToString();
            elipsaR2.Text = radius2.ToString();
            elipsaX.Text = random.Next(0 + radius1, 1000 - radius1).ToString();
            elipsaY.Text = random.Next(0 + radius2, 700 - radius2).ToString();
        }

        private void pravokutnikSend_Click(object sender, EventArgs e)
        {
            if (pravokutnikBoja.Text.Length == 0 || pravokutnik1x.Text.Length == 0 || pravokutnik1y.Text.Length == 0 || pravokutnikSirina.Text.Length == 0 || pravokutnikVisina.Text.Length == 0)
            {
                MessageBox.Show("Some elements are missing! Make sure you enter all values.");
                return;
            }
            if (!connectionUp)
            {
                MessageBox.Show("Connect to the server and try again!");
                return;
            }
            string message = "Rectangle " + pravokutnikBoja.Text.ToString() + " " + pravokutnik1x.Text.ToString() + " " + pravokutnik1y.Text.ToString() + " " + pravokutnikVisina.Text.ToString() + " " + pravokutnikSirina.Text.ToString();
            int byteCount = Encoding.ASCII.GetByteCount(message); //broj bajtova u poruci
            byte[] sendData = new byte[byteCount]; //Bajt podaci koji se šalju
            sendData = Encoding.ASCII.GetBytes(message);
            stream.Write(sendData, 0, sendData.Length);
            pravokutnikBoja.Text = "";
            pravokutnik1x.Text = "";
            pravokutnik1y.Text = "";
            pravokutnikSirina.Text = "";
            pravokutnikVisina.Text = "";
        }

        private void kruznicaSend_Click(object sender, EventArgs e)
        {
            if (kruznicaBoja.Text.Length == 0 || kruznicaX.Text.Length == 0 || kruznicaY.Text.Length == 0 || kruznicaR.Text.Length == 0)
            {
                MessageBox.Show("Some elements are missing! Make sure you enter all values.");
                return;
            }
            if (!connectionUp)
            {
                MessageBox.Show("Connect to the server and try again!");
                return;
            }
            string message = "Circle " + kruznicaBoja.Text.ToString() + " " + kruznicaX.Text.ToString() + " " + kruznicaY.Text.ToString() + " " + kruznicaR.Text.ToString();
            int byteCount = Encoding.ASCII.GetByteCount(message); //broj bajtova u poruci
            byte[] sendData = new byte[byteCount]; //Bajt podaci koji se šalju
            sendData = Encoding.ASCII.GetBytes(message);
            stream.Write(sendData, 0, sendData.Length);
            kruznicaBoja.Text = "";
            kruznicaR.Text = "";
            kruznicaX.Text = "";
            kruznicaY.Text = "";
        }

        private void elipsaSend_Click(object sender, EventArgs e)
        {
            if (elipsaBoja.Text.Length == 0 || elipsaX.Text.Length == 0 || elipsaY.Text.Length == 0 || elipsaR1.Text.Length == 0 || elipsaR2.Text.Length == 0)
            {
                MessageBox.Show("Some elements are missing! Make sure you enter all values.");
                return;
            }
            if (!connectionUp)
            {
                MessageBox.Show("Connect to the server and try again!");
                return;
            }
            string message = "Ellipse " + elipsaBoja.Text.ToString() + " " + elipsaX.Text.ToString() + " " + elipsaY.Text.ToString() + " " + elipsaR1.Text.ToString() + " " + elipsaR2.Text.ToString();
            int byteCount = Encoding.ASCII.GetByteCount(message); //broj bajtova u poruci
            byte[] sendData = new byte[byteCount]; //Bajt podaci koji se šalju
            sendData = Encoding.ASCII.GetBytes(message);
            stream.Write(sendData, 0, sendData.Length);
            elipsaBoja.Text = "";
            elipsaX.Text = "";
            elipsaY.Text = "";
            elipsaR1.Text = "";
            elipsaR2.Text = "";
        }

        private void poligonAdd_Click(object sender, EventArgs e)
        {
            if (poligonX.Text.Length == 0 || poligonY.Text.Length == 0)
            {
                MessageBox.Show("Some elements are missing! Make sure you enter all values.");
                return;
            }
            if (!connectionUp)
            {
                MessageBox.Show("Connect to the server and try again!");
                return;
            }
            richTextBox1.AppendText(poligonX.Text.ToString() + " " + poligonY.Text.ToString() + " ");
            poligonX.Text = "";
            poligonY.Text = "";
        }

        private void poligonSend_Click(object sender, EventArgs e)
        {
            if (poligonBoja.Text.Length == 0 || richTextBox1.Text.Split().Count() < 6)
            {
                MessageBox.Show("Some elements are missing! Make sure you enter all values. To draw a polygon specify at least 3 points.");
                return;
            }
            if (!connectionUp)
            {
                MessageBox.Show("Connect to the server and try again!");
                return;
            }
            string message = "Polygon " + poligonBoja.Text.ToString() + " " + richTextBox1.Text.ToString();
            int byteCount = Encoding.ASCII.GetByteCount(message); //broj bajtova u poruci
            byte[] sendData = new byte[byteCount]; //Bajt podaci koji se šalju
            sendData = Encoding.ASCII.GetBytes(message);
            stream.Write(sendData, 0, sendData.Length);
            poligonBoja.Text = "";
            richTextBox1.Clear();
        }

        private void clearBtn_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
        }
    }
}
