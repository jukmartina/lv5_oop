﻿
namespace LV5_mmj
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.disconnectBtn = new System.Windows.Forms.Button();
            this.connectBtn = new System.Windows.Forms.Button();
            this.hostPort = new System.Windows.Forms.TextBox();
            this.hostName = new System.Windows.Forms.TextBox();
            this.hostPortLabel = new System.Windows.Forms.Label();
            this.hostNameLabel = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.linijaSend = new System.Windows.Forms.Button();
            this.linijaRandom = new System.Windows.Forms.Button();
            this.linijaBoja = new System.Windows.Forms.TextBox();
            this.linija2y = new System.Windows.Forms.TextBox();
            this.linija2x = new System.Windows.Forms.TextBox();
            this.linija1y = new System.Windows.Forms.TextBox();
            this.linija1x = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.trokut3y = new System.Windows.Forms.TextBox();
            this.trokut3x = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.trokutSend = new System.Windows.Forms.Button();
            this.trokutRandom = new System.Windows.Forms.Button();
            this.trokutBoja = new System.Windows.Forms.TextBox();
            this.trokut2y = new System.Windows.Forms.TextBox();
            this.trokut2x = new System.Windows.Forms.TextBox();
            this.trokut1y = new System.Windows.Forms.TextBox();
            this.trokut1x = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.pravokutnikSend = new System.Windows.Forms.Button();
            this.pravokutnikRandom = new System.Windows.Forms.Button();
            this.pravokutnikBoja = new System.Windows.Forms.TextBox();
            this.pravokutnikSirina = new System.Windows.Forms.TextBox();
            this.pravokutnikVisina = new System.Windows.Forms.TextBox();
            this.pravokutnik1y = new System.Windows.Forms.TextBox();
            this.pravokutnik1x = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.poligonSend = new System.Windows.Forms.Button();
            this.poligonRandom = new System.Windows.Forms.Button();
            this.poligonBoja = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.poligonAdd = new System.Windows.Forms.Button();
            this.poligonY = new System.Windows.Forms.TextBox();
            this.poligonX = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.kruznicaSend = new System.Windows.Forms.Button();
            this.kruznicaRandom = new System.Windows.Forms.Button();
            this.kruznicaBoja = new System.Windows.Forms.TextBox();
            this.kruznicaR = new System.Windows.Forms.TextBox();
            this.kruznicaY = new System.Windows.Forms.TextBox();
            this.kruznicaX = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.elipsaR2 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.elipsaSend = new System.Windows.Forms.Button();
            this.elipsaRandom = new System.Windows.Forms.Button();
            this.elipsaBoja = new System.Windows.Forms.TextBox();
            this.elipsaR1 = new System.Windows.Forms.TextBox();
            this.elipsaY = new System.Windows.Forms.TextBox();
            this.elipsaX = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.connectionStatusLabel = new System.Windows.Forms.Label();
            this.clearBtn = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Location = new System.Drawing.Point(17, 16);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(809, 523);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.disconnectBtn);
            this.tabPage1.Controls.Add(this.connectBtn);
            this.tabPage1.Controls.Add(this.hostPort);
            this.tabPage1.Controls.Add(this.hostName);
            this.tabPage1.Controls.Add(this.hostPortLabel);
            this.tabPage1.Controls.Add(this.hostNameLabel);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage1.Size = new System.Drawing.Size(801, 494);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // disconnectBtn
            // 
            this.disconnectBtn.Location = new System.Drawing.Point(451, 352);
            this.disconnectBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.disconnectBtn.Name = "disconnectBtn";
            this.disconnectBtn.Size = new System.Drawing.Size(249, 86);
            this.disconnectBtn.TabIndex = 5;
            this.disconnectBtn.Text = "Close connection";
            this.disconnectBtn.UseVisualStyleBackColor = true;
            this.disconnectBtn.Click += new System.EventHandler(this.disconnectBtn_Click);
            // 
            // connectBtn
            // 
            this.connectBtn.Location = new System.Drawing.Point(80, 352);
            this.connectBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.connectBtn.Name = "connectBtn";
            this.connectBtn.Size = new System.Drawing.Size(220, 86);
            this.connectBtn.TabIndex = 4;
            this.connectBtn.Text = "Connect";
            this.connectBtn.UseVisualStyleBackColor = true;
            this.connectBtn.Click += new System.EventHandler(this.connectBtn_Click);
            // 
            // hostPort
            // 
            this.hostPort.Location = new System.Drawing.Point(116, 76);
            this.hostPort.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.hostPort.Name = "hostPort";
            this.hostPort.Size = new System.Drawing.Size(132, 22);
            this.hostPort.TabIndex = 3;
            // 
            // hostName
            // 
            this.hostName.Location = new System.Drawing.Point(116, 31);
            this.hostName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.hostName.Name = "hostName";
            this.hostName.Size = new System.Drawing.Size(132, 22);
            this.hostName.TabIndex = 2;
            // 
            // hostPortLabel
            // 
            this.hostPortLabel.AutoSize = true;
            this.hostPortLabel.Location = new System.Drawing.Point(36, 76);
            this.hostPortLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.hostPortLabel.Name = "hostPortLabel";
            this.hostPortLabel.Size = new System.Drawing.Size(64, 16);
            this.hostPortLabel.TabIndex = 1;
            this.hostPortLabel.Text = "Host port:";
            // 
            // hostNameLabel
            // 
            this.hostNameLabel.AutoSize = true;
            this.hostNameLabel.Location = new System.Drawing.Point(25, 31);
            this.hostNameLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.hostNameLabel.Name = "hostNameLabel";
            this.hostNameLabel.Size = new System.Drawing.Size(75, 16);
            this.hostNameLabel.TabIndex = 0;
            this.hostNameLabel.Text = "Host name:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.linijaSend);
            this.tabPage2.Controls.Add(this.linijaRandom);
            this.tabPage2.Controls.Add(this.linijaBoja);
            this.tabPage2.Controls.Add(this.linija2y);
            this.tabPage2.Controls.Add(this.linija2x);
            this.tabPage2.Controls.Add(this.linija1y);
            this.tabPage2.Controls.Add(this.linija1x);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage2.Size = new System.Drawing.Size(801, 494);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // linijaSend
            // 
            this.linijaSend.Location = new System.Drawing.Point(215, 299);
            this.linijaSend.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.linijaSend.Name = "linijaSend";
            this.linijaSend.Size = new System.Drawing.Size(281, 95);
            this.linijaSend.TabIndex = 11;
            this.linijaSend.Text = "Send";
            this.linijaSend.UseVisualStyleBackColor = true;
            this.linijaSend.Click += new System.EventHandler(this.linijaSend_Click);
            // 
            // linijaRandom
            // 
            this.linijaRandom.Location = new System.Drawing.Point(363, 139);
            this.linijaRandom.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.linijaRandom.Name = "linijaRandom";
            this.linijaRandom.Size = new System.Drawing.Size(100, 28);
            this.linijaRandom.TabIndex = 10;
            this.linijaRandom.Text = "Random";
            this.linijaRandom.UseVisualStyleBackColor = true;
            this.linijaRandom.Click += new System.EventHandler(this.linijaRandom_Click);
            // 
            // linijaBoja
            // 
            this.linijaBoja.Location = new System.Drawing.Point(363, 86);
            this.linijaBoja.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.linijaBoja.Name = "linijaBoja";
            this.linijaBoja.Size = new System.Drawing.Size(132, 22);
            this.linijaBoja.TabIndex = 9;
            // 
            // linija2y
            // 
            this.linija2y.Location = new System.Drawing.Point(120, 143);
            this.linija2y.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.linija2y.Name = "linija2y";
            this.linija2y.Size = new System.Drawing.Size(132, 22);
            this.linija2y.TabIndex = 8;
            // 
            // linija2x
            // 
            this.linija2x.Location = new System.Drawing.Point(120, 107);
            this.linija2x.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.linija2x.Name = "linija2x";
            this.linija2x.Size = new System.Drawing.Size(132, 22);
            this.linija2x.TabIndex = 7;
            // 
            // linija1y
            // 
            this.linija1y.Location = new System.Drawing.Point(120, 65);
            this.linija1y.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.linija1y.Name = "linija1y";
            this.linija1y.Size = new System.Drawing.Size(132, 22);
            this.linija1y.TabIndex = 6;
            // 
            // linija1x
            // 
            this.linija1x.Location = new System.Drawing.Point(120, 23);
            this.linija1x.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.linija1x.Name = "linija1x";
            this.linija1x.Size = new System.Drawing.Size(132, 22);
            this.linija1x.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(312, 86);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 16);
            this.label5.TabIndex = 4;
            this.label5.Text = "Boja:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 153);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "Tocka 2 - Y:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 107);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Tocka 2 - X:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 65);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tocka 1 - Y:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 23);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tocka 1 - X:";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.trokut3y);
            this.tabPage3.Controls.Add(this.trokut3x);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Controls.Add(this.trokutSend);
            this.tabPage3.Controls.Add(this.trokutRandom);
            this.tabPage3.Controls.Add(this.trokutBoja);
            this.tabPage3.Controls.Add(this.trokut2y);
            this.tabPage3.Controls.Add(this.trokut2x);
            this.tabPage3.Controls.Add(this.trokut1y);
            this.tabPage3.Controls.Add(this.trokut1x);
            this.tabPage3.Controls.Add(this.label6);
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage3.Size = new System.Drawing.Size(801, 494);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "tabPage3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // trokut3y
            // 
            this.trokut3y.Location = new System.Drawing.Point(121, 238);
            this.trokut3y.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.trokut3y.Name = "trokut3y";
            this.trokut3y.Size = new System.Drawing.Size(132, 22);
            this.trokut3y.TabIndex = 27;
            // 
            // trokut3x
            // 
            this.trokut3x.Location = new System.Drawing.Point(121, 198);
            this.trokut3x.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.trokut3x.Name = "trokut3x";
            this.trokut3x.Size = new System.Drawing.Size(132, 22);
            this.trokut3x.TabIndex = 26;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(24, 247);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(78, 16);
            this.label11.TabIndex = 25;
            this.label11.Text = "Tocka 3 - Y:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(24, 202);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(77, 16);
            this.label12.TabIndex = 24;
            this.label12.Text = "Tocka 3 - X:";
            // 
            // trokutSend
            // 
            this.trokutSend.Location = new System.Drawing.Point(216, 300);
            this.trokutSend.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.trokutSend.Name = "trokutSend";
            this.trokutSend.Size = new System.Drawing.Size(281, 95);
            this.trokutSend.TabIndex = 23;
            this.trokutSend.Text = "Send";
            this.trokutSend.UseVisualStyleBackColor = true;
            this.trokutSend.Click += new System.EventHandler(this.trokutSend_Click);
            // 
            // trokutRandom
            // 
            this.trokutRandom.Location = new System.Drawing.Point(364, 139);
            this.trokutRandom.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.trokutRandom.Name = "trokutRandom";
            this.trokutRandom.Size = new System.Drawing.Size(100, 28);
            this.trokutRandom.TabIndex = 22;
            this.trokutRandom.Text = "Random";
            this.trokutRandom.UseVisualStyleBackColor = true;
            this.trokutRandom.Click += new System.EventHandler(this.trokutRandom_Click);
            // 
            // trokutBoja
            // 
            this.trokutBoja.Location = new System.Drawing.Point(364, 86);
            this.trokutBoja.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.trokutBoja.Name = "trokutBoja";
            this.trokutBoja.Size = new System.Drawing.Size(132, 22);
            this.trokutBoja.TabIndex = 21;
            // 
            // trokut2y
            // 
            this.trokut2y.Location = new System.Drawing.Point(120, 149);
            this.trokut2y.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.trokut2y.Name = "trokut2y";
            this.trokut2y.Size = new System.Drawing.Size(132, 22);
            this.trokut2y.TabIndex = 20;
            // 
            // trokut2x
            // 
            this.trokut2x.Location = new System.Drawing.Point(121, 107);
            this.trokut2x.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.trokut2x.Name = "trokut2x";
            this.trokut2x.Size = new System.Drawing.Size(132, 22);
            this.trokut2x.TabIndex = 19;
            // 
            // trokut1y
            // 
            this.trokut1y.Location = new System.Drawing.Point(121, 65);
            this.trokut1y.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.trokut1y.Name = "trokut1y";
            this.trokut1y.Size = new System.Drawing.Size(132, 22);
            this.trokut1y.TabIndex = 18;
            // 
            // trokut1x
            // 
            this.trokut1x.Location = new System.Drawing.Point(121, 23);
            this.trokut1x.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.trokut1x.Name = "trokut1x";
            this.trokut1x.Size = new System.Drawing.Size(132, 22);
            this.trokut1x.TabIndex = 17;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(313, 86);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 16);
            this.label6.TabIndex = 16;
            this.label6.Text = "Boja:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(24, 153);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 16);
            this.label7.TabIndex = 15;
            this.label7.Text = "Tocka 2 - Y:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(24, 107);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 16);
            this.label8.TabIndex = 14;
            this.label8.Text = "Tocka 2 - X:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(24, 65);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(78, 16);
            this.label9.TabIndex = 13;
            this.label9.Text = "Tocka 1 - Y:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(24, 23);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 16);
            this.label10.TabIndex = 12;
            this.label10.Text = "Tocka 1 - X:";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.pravokutnikSend);
            this.tabPage4.Controls.Add(this.pravokutnikRandom);
            this.tabPage4.Controls.Add(this.pravokutnikBoja);
            this.tabPage4.Controls.Add(this.pravokutnikSirina);
            this.tabPage4.Controls.Add(this.pravokutnikVisina);
            this.tabPage4.Controls.Add(this.pravokutnik1y);
            this.tabPage4.Controls.Add(this.pravokutnik1x);
            this.tabPage4.Controls.Add(this.label13);
            this.tabPage4.Controls.Add(this.label14);
            this.tabPage4.Controls.Add(this.label15);
            this.tabPage4.Controls.Add(this.label16);
            this.tabPage4.Controls.Add(this.label17);
            this.tabPage4.Location = new System.Drawing.Point(4, 25);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage4.Size = new System.Drawing.Size(801, 494);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "tabPage4";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // pravokutnikSend
            // 
            this.pravokutnikSend.Location = new System.Drawing.Point(215, 298);
            this.pravokutnikSend.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pravokutnikSend.Name = "pravokutnikSend";
            this.pravokutnikSend.Size = new System.Drawing.Size(281, 95);
            this.pravokutnikSend.TabIndex = 23;
            this.pravokutnikSend.Text = "Send";
            this.pravokutnikSend.UseVisualStyleBackColor = true;
            this.pravokutnikSend.Click += new System.EventHandler(this.pravokutnikSend_Click);
            // 
            // pravokutnikRandom
            // 
            this.pravokutnikRandom.Location = new System.Drawing.Point(363, 138);
            this.pravokutnikRandom.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pravokutnikRandom.Name = "pravokutnikRandom";
            this.pravokutnikRandom.Size = new System.Drawing.Size(100, 28);
            this.pravokutnikRandom.TabIndex = 22;
            this.pravokutnikRandom.Text = "Random";
            this.pravokutnikRandom.UseVisualStyleBackColor = true;
            this.pravokutnikRandom.Click += new System.EventHandler(this.pravokutnikRandom_Click);
            // 
            // pravokutnikBoja
            // 
            this.pravokutnikBoja.Location = new System.Drawing.Point(363, 85);
            this.pravokutnikBoja.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pravokutnikBoja.Name = "pravokutnikBoja";
            this.pravokutnikBoja.Size = new System.Drawing.Size(132, 22);
            this.pravokutnikBoja.TabIndex = 21;
            // 
            // pravokutnikSirina
            // 
            this.pravokutnikSirina.Location = new System.Drawing.Point(120, 142);
            this.pravokutnikSirina.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pravokutnikSirina.Name = "pravokutnikSirina";
            this.pravokutnikSirina.Size = new System.Drawing.Size(132, 22);
            this.pravokutnikSirina.TabIndex = 20;
            // 
            // pravokutnikVisina
            // 
            this.pravokutnikVisina.Location = new System.Drawing.Point(120, 106);
            this.pravokutnikVisina.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pravokutnikVisina.Name = "pravokutnikVisina";
            this.pravokutnikVisina.Size = new System.Drawing.Size(132, 22);
            this.pravokutnikVisina.TabIndex = 19;
            this.pravokutnikVisina.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // pravokutnik1y
            // 
            this.pravokutnik1y.Location = new System.Drawing.Point(120, 64);
            this.pravokutnik1y.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pravokutnik1y.Name = "pravokutnik1y";
            this.pravokutnik1y.Size = new System.Drawing.Size(132, 22);
            this.pravokutnik1y.TabIndex = 18;
            // 
            // pravokutnik1x
            // 
            this.pravokutnik1x.Location = new System.Drawing.Point(120, 22);
            this.pravokutnik1x.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pravokutnik1x.Name = "pravokutnik1x";
            this.pravokutnik1x.Size = new System.Drawing.Size(132, 22);
            this.pravokutnik1x.TabIndex = 17;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(312, 85);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(38, 16);
            this.label13.TabIndex = 16;
            this.label13.Text = "Boja:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(23, 151);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(44, 16);
            this.label14.TabIndex = 15;
            this.label14.Text = "Sirina:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(23, 106);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(47, 16);
            this.label15.TabIndex = 14;
            this.label15.Text = "Visina:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(23, 64);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(78, 16);
            this.label16.TabIndex = 13;
            this.label16.Text = "Tocka 1 - Y:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(23, 22);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(77, 16);
            this.label17.TabIndex = 12;
            this.label17.Text = "Tocka 1 - X:";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.clearBtn);
            this.tabPage5.Controls.Add(this.poligonSend);
            this.tabPage5.Controls.Add(this.poligonRandom);
            this.tabPage5.Controls.Add(this.poligonBoja);
            this.tabPage5.Controls.Add(this.label29);
            this.tabPage5.Controls.Add(this.poligonAdd);
            this.tabPage5.Controls.Add(this.poligonY);
            this.tabPage5.Controls.Add(this.poligonX);
            this.tabPage5.Controls.Add(this.label28);
            this.tabPage5.Controls.Add(this.label27);
            this.tabPage5.Controls.Add(this.richTextBox1);
            this.tabPage5.Location = new System.Drawing.Point(4, 25);
            this.tabPage5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage5.Size = new System.Drawing.Size(801, 494);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "tabPage5";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // poligonSend
            // 
            this.poligonSend.Location = new System.Drawing.Point(217, 368);
            this.poligonSend.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.poligonSend.Name = "poligonSend";
            this.poligonSend.Size = new System.Drawing.Size(281, 95);
            this.poligonSend.TabIndex = 26;
            this.poligonSend.Text = "Send";
            this.poligonSend.UseVisualStyleBackColor = true;
            this.poligonSend.Click += new System.EventHandler(this.poligonSend_Click);
            // 
            // poligonRandom
            // 
            this.poligonRandom.Location = new System.Drawing.Point(563, 313);
            this.poligonRandom.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.poligonRandom.Name = "poligonRandom";
            this.poligonRandom.Size = new System.Drawing.Size(100, 28);
            this.poligonRandom.TabIndex = 25;
            this.poligonRandom.Text = "Random";
            this.poligonRandom.UseVisualStyleBackColor = true;
            this.poligonRandom.Click += new System.EventHandler(this.poligonRandom_Click);
            // 
            // poligonBoja
            // 
            this.poligonBoja.Location = new System.Drawing.Point(563, 260);
            this.poligonBoja.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.poligonBoja.Name = "poligonBoja";
            this.poligonBoja.Size = new System.Drawing.Size(132, 22);
            this.poligonBoja.TabIndex = 24;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(512, 260);
            this.label29.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(38, 16);
            this.label29.TabIndex = 23;
            this.label29.Text = "Boja:";
            // 
            // poligonAdd
            // 
            this.poligonAdd.Location = new System.Drawing.Point(555, 145);
            this.poligonAdd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.poligonAdd.Name = "poligonAdd";
            this.poligonAdd.Size = new System.Drawing.Size(157, 28);
            this.poligonAdd.TabIndex = 5;
            this.poligonAdd.Text = "Add point";
            this.poligonAdd.UseVisualStyleBackColor = true;
            this.poligonAdd.Click += new System.EventHandler(this.poligonAdd_Click);
            // 
            // poligonY
            // 
            this.poligonY.Location = new System.Drawing.Point(579, 82);
            this.poligonY.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.poligonY.Name = "poligonY";
            this.poligonY.Size = new System.Drawing.Size(132, 22);
            this.poligonY.TabIndex = 4;
            // 
            // poligonX
            // 
            this.poligonX.Location = new System.Drawing.Point(579, 34);
            this.poligonX.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.poligonX.Name = "poligonX";
            this.poligonX.Size = new System.Drawing.Size(132, 22);
            this.poligonX.TabIndex = 3;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(477, 82);
            this.label28.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(68, 16);
            this.label28.TabIndex = 2;
            this.label28.Text = "Tocka - Y:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(473, 34);
            this.label27.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(67, 16);
            this.label27.TabIndex = 1;
            this.label27.Text = "Tocka - X:";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(21, 22);
            this.richTextBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(421, 160);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.kruznicaSend);
            this.tabPage6.Controls.Add(this.kruznicaRandom);
            this.tabPage6.Controls.Add(this.kruznicaBoja);
            this.tabPage6.Controls.Add(this.kruznicaR);
            this.tabPage6.Controls.Add(this.kruznicaY);
            this.tabPage6.Controls.Add(this.kruznicaX);
            this.tabPage6.Controls.Add(this.label18);
            this.tabPage6.Controls.Add(this.label20);
            this.tabPage6.Controls.Add(this.label21);
            this.tabPage6.Controls.Add(this.label22);
            this.tabPage6.Location = new System.Drawing.Point(4, 25);
            this.tabPage6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage6.Size = new System.Drawing.Size(801, 494);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "tabPage6";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // kruznicaSend
            // 
            this.kruznicaSend.Location = new System.Drawing.Point(217, 300);
            this.kruznicaSend.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.kruznicaSend.Name = "kruznicaSend";
            this.kruznicaSend.Size = new System.Drawing.Size(281, 95);
            this.kruznicaSend.TabIndex = 23;
            this.kruznicaSend.Text = "Send";
            this.kruznicaSend.UseVisualStyleBackColor = true;
            this.kruznicaSend.Click += new System.EventHandler(this.kruznicaSend_Click);
            // 
            // kruznicaRandom
            // 
            this.kruznicaRandom.Location = new System.Drawing.Point(365, 140);
            this.kruznicaRandom.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.kruznicaRandom.Name = "kruznicaRandom";
            this.kruznicaRandom.Size = new System.Drawing.Size(100, 28);
            this.kruznicaRandom.TabIndex = 22;
            this.kruznicaRandom.Text = "Random";
            this.kruznicaRandom.UseVisualStyleBackColor = true;
            this.kruznicaRandom.Click += new System.EventHandler(this.kruznicaRandom_Click);
            // 
            // kruznicaBoja
            // 
            this.kruznicaBoja.Location = new System.Drawing.Point(365, 87);
            this.kruznicaBoja.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.kruznicaBoja.Name = "kruznicaBoja";
            this.kruznicaBoja.Size = new System.Drawing.Size(132, 22);
            this.kruznicaBoja.TabIndex = 21;
            // 
            // kruznicaR
            // 
            this.kruznicaR.Location = new System.Drawing.Point(123, 108);
            this.kruznicaR.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.kruznicaR.Name = "kruznicaR";
            this.kruznicaR.Size = new System.Drawing.Size(132, 22);
            this.kruznicaR.TabIndex = 19;
            // 
            // kruznicaY
            // 
            this.kruznicaY.Location = new System.Drawing.Point(123, 66);
            this.kruznicaY.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.kruznicaY.Name = "kruznicaY";
            this.kruznicaY.Size = new System.Drawing.Size(132, 22);
            this.kruznicaY.TabIndex = 18;
            // 
            // kruznicaX
            // 
            this.kruznicaX.Location = new System.Drawing.Point(123, 25);
            this.kruznicaX.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.kruznicaX.Name = "kruznicaX";
            this.kruznicaX.Size = new System.Drawing.Size(132, 22);
            this.kruznicaX.TabIndex = 17;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(315, 87);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(38, 16);
            this.label18.TabIndex = 16;
            this.label18.Text = "Boja:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(25, 108);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(53, 16);
            this.label20.TabIndex = 14;
            this.label20.Text = "Radius:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(25, 66);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(68, 16);
            this.label21.TabIndex = 13;
            this.label21.Text = "Centar - Y:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(25, 25);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(67, 16);
            this.label22.TabIndex = 12;
            this.label22.Text = "Centar - X:";
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.elipsaR2);
            this.tabPage7.Controls.Add(this.label26);
            this.tabPage7.Controls.Add(this.elipsaSend);
            this.tabPage7.Controls.Add(this.elipsaRandom);
            this.tabPage7.Controls.Add(this.elipsaBoja);
            this.tabPage7.Controls.Add(this.elipsaR1);
            this.tabPage7.Controls.Add(this.elipsaY);
            this.tabPage7.Controls.Add(this.elipsaX);
            this.tabPage7.Controls.Add(this.label19);
            this.tabPage7.Controls.Add(this.label23);
            this.tabPage7.Controls.Add(this.label24);
            this.tabPage7.Controls.Add(this.label25);
            this.tabPage7.Location = new System.Drawing.Point(4, 25);
            this.tabPage7.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage7.Size = new System.Drawing.Size(801, 494);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "tabPage7";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // elipsaR2
            // 
            this.elipsaR2.Location = new System.Drawing.Point(120, 151);
            this.elipsaR2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.elipsaR2.Name = "elipsaR2";
            this.elipsaR2.Size = new System.Drawing.Size(132, 22);
            this.elipsaR2.TabIndex = 35;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(23, 151);
            this.label26.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(63, 16);
            this.label26.TabIndex = 34;
            this.label26.Text = "Radius 2:";
            // 
            // elipsaSend
            // 
            this.elipsaSend.Location = new System.Drawing.Point(215, 299);
            this.elipsaSend.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.elipsaSend.Name = "elipsaSend";
            this.elipsaSend.Size = new System.Drawing.Size(281, 95);
            this.elipsaSend.TabIndex = 33;
            this.elipsaSend.Text = "Send";
            this.elipsaSend.UseVisualStyleBackColor = true;
            this.elipsaSend.Click += new System.EventHandler(this.elipsaSend_Click);
            // 
            // elipsaRandom
            // 
            this.elipsaRandom.Location = new System.Drawing.Point(363, 139);
            this.elipsaRandom.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.elipsaRandom.Name = "elipsaRandom";
            this.elipsaRandom.Size = new System.Drawing.Size(100, 28);
            this.elipsaRandom.TabIndex = 32;
            this.elipsaRandom.Text = "Random";
            this.elipsaRandom.UseVisualStyleBackColor = true;
            this.elipsaRandom.Click += new System.EventHandler(this.elipsaRandom_Click);
            // 
            // elipsaBoja
            // 
            this.elipsaBoja.Location = new System.Drawing.Point(363, 86);
            this.elipsaBoja.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.elipsaBoja.Name = "elipsaBoja";
            this.elipsaBoja.Size = new System.Drawing.Size(132, 22);
            this.elipsaBoja.TabIndex = 31;
            // 
            // elipsaR1
            // 
            this.elipsaR1.Location = new System.Drawing.Point(120, 107);
            this.elipsaR1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.elipsaR1.Name = "elipsaR1";
            this.elipsaR1.Size = new System.Drawing.Size(132, 22);
            this.elipsaR1.TabIndex = 30;
            // 
            // elipsaY
            // 
            this.elipsaY.Location = new System.Drawing.Point(120, 65);
            this.elipsaY.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.elipsaY.Name = "elipsaY";
            this.elipsaY.Size = new System.Drawing.Size(132, 22);
            this.elipsaY.TabIndex = 29;
            // 
            // elipsaX
            // 
            this.elipsaX.Location = new System.Drawing.Point(120, 23);
            this.elipsaX.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.elipsaX.Name = "elipsaX";
            this.elipsaX.Size = new System.Drawing.Size(132, 22);
            this.elipsaX.TabIndex = 28;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(312, 86);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(38, 16);
            this.label19.TabIndex = 27;
            this.label19.Text = "Boja:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(23, 107);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(63, 16);
            this.label23.TabIndex = 26;
            this.label23.Text = "Radius 1:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(23, 65);
            this.label24.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(68, 16);
            this.label24.TabIndex = 25;
            this.label24.Text = "Centar - Y:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(23, 23);
            this.label25.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(67, 16);
            this.label25.TabIndex = 24;
            this.label25.Text = "Centar - X:";
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(872, 245);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(155, 138);
            this.panel1.TabIndex = 2;
            // 
            // connectionStatusLabel
            // 
            this.connectionStatusLabel.AutoSize = true;
            this.connectionStatusLabel.Location = new System.Drawing.Point(887, 210);
            this.connectionStatusLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.connectionStatusLabel.Name = "connectionStatusLabel";
            this.connectionStatusLabel.Size = new System.Drawing.Size(115, 16);
            this.connectionStatusLabel.TabIndex = 3;
            this.connectionStatusLabel.Text = "Connection status:";
            // 
            // clearBtn
            // 
            this.clearBtn.Location = new System.Drawing.Point(21, 204);
            this.clearBtn.Name = "clearBtn";
            this.clearBtn.Size = new System.Drawing.Size(160, 23);
            this.clearBtn.TabIndex = 27;
            this.clearBtn.Text = "Clear points";
            this.clearBtn.UseVisualStyleBackColor = true;
            this.clearBtn.Click += new System.EventHandler(this.clearBtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 554);
            this.Controls.Add(this.connectionStatusLabel);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tabControl1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label connectionStatusLabel;
        private System.Windows.Forms.Label hostPortLabel;
        private System.Windows.Forms.Label hostNameLabel;
        private System.Windows.Forms.Button disconnectBtn;
        private System.Windows.Forms.Button connectBtn;
        private System.Windows.Forms.TextBox hostPort;
        private System.Windows.Forms.TextBox hostName;
        private System.Windows.Forms.Button linijaSend;
        private System.Windows.Forms.Button linijaRandom;
        private System.Windows.Forms.TextBox linijaBoja;
        private System.Windows.Forms.TextBox linija2y;
        private System.Windows.Forms.TextBox linija2x;
        private System.Windows.Forms.TextBox linija1y;
        private System.Windows.Forms.TextBox linija1x;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox trokut3y;
        private System.Windows.Forms.TextBox trokut3x;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button trokutSend;
        private System.Windows.Forms.Button trokutRandom;
        private System.Windows.Forms.TextBox trokutBoja;
        private System.Windows.Forms.TextBox trokut2y;
        private System.Windows.Forms.TextBox trokut2x;
        private System.Windows.Forms.TextBox trokut1y;
        private System.Windows.Forms.TextBox trokut1x;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button pravokutnikSend;
        private System.Windows.Forms.Button pravokutnikRandom;
        private System.Windows.Forms.TextBox pravokutnikBoja;
        private System.Windows.Forms.TextBox pravokutnikSirina;
        private System.Windows.Forms.TextBox pravokutnikVisina;
        private System.Windows.Forms.TextBox pravokutnik1y;
        private System.Windows.Forms.TextBox pravokutnik1x;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button kruznicaSend;
        private System.Windows.Forms.Button kruznicaRandom;
        private System.Windows.Forms.TextBox kruznicaBoja;
        private System.Windows.Forms.TextBox kruznicaR;
        private System.Windows.Forms.TextBox kruznicaY;
        private System.Windows.Forms.TextBox kruznicaX;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button poligonSend;
        private System.Windows.Forms.Button poligonRandom;
        private System.Windows.Forms.TextBox poligonBoja;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Button poligonAdd;
        private System.Windows.Forms.TextBox poligonY;
        private System.Windows.Forms.TextBox poligonX;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.TextBox elipsaR2;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button elipsaSend;
        private System.Windows.Forms.Button elipsaRandom;
        private System.Windows.Forms.TextBox elipsaBoja;
        private System.Windows.Forms.TextBox elipsaR1;
        private System.Windows.Forms.TextBox elipsaY;
        private System.Windows.Forms.TextBox elipsaX;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button clearBtn;
    }
}

