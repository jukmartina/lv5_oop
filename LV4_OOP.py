
from re import X
import tkinter as tk
from tkinter import filedialog
import socket
import threading as thr

root= tk.Tk()
root.title("LV4 MMJ")
canvas = tk.Canvas(root, bg="#C0C0C0", width=1000, height=700)
canvas.pack()

class GrafObj():
    boja = "black"
    tocka1 = [0.0, 0.0]
    def __init__(self, boja, tocka1) -> None:
        self.boja = boja
        self.tocka1 = tocka1
    def setColor(self, boja) -> None:
        self.boja = boja
    def getColor(self) -> str:
        return self.boja
    def draw(self) -> None:
        pass

class Linija(GrafObj):
    tocka2 = [100.0, 100.0]
    def __init__(self, boja, tocka1, tocka2) -> None:
        super().__init__(boja, tocka1)
        self.tocka2 = tocka2
    def draw(self) -> None:
        canvas.create_line(self.tocka1[0], self.tocka1[1], self.tocka2[0], self.tocka2[1], fill=self.boja)

class Pravokutnik(GrafObj):
    visina = 0
    sirina = 0
    def __init__(self, boja, tocka1, visina, sirina) -> None:
        super().__init__(boja, tocka1)
        self.visina = visina
        self.sirina = sirina
    def draw(self) -> None:
        canvas.create_rectangle(self.tocka1[0], self.tocka1[1], self.tocka1[0]+self.sirina, self.tocka1[1]+ self.visina, outline=self.boja, )

class Poligon(GrafObj):
    tocke = []
    def __init__(self, boja, tocka1, tocke) -> None:
        super().__init__(boja, tocka1)
        self.tocke = tocke
    def draw(self) -> None:
        canvas.create_polygon(self.tocka1[0], self.tocka1[1], self.tocke, outline=self.boja, fill='')

class Kruznica(GrafObj):
    radius = 0.0
    def __init__(self, boja, tocka1, radius) -> None:
        super().__init__(boja, tocka1)
        self.radius = radius
    def draw(self) -> None:
        canvas.create_oval(self.tocka1[0]-self.radius, self.tocka1[1]-self.radius, self.tocka1[0]+self.radius, self.tocka1[1]+self.radius, outline=self.boja)

class Trokut(Linija):
    tocka3 = [50.0, 50.0]
    def __init__(self, boja, tocka1, tocka2, tocka3) -> None:
        super().__init__(boja, tocka1, tocka2)
        self.tocka3 = tocka3
    def draw(self) -> None:
        linija = Linija(self.boja, self.tocka1, self.tocka2)
        linija.draw()
        linija = Linija(self.boja, self.tocka2, self.tocka3)
        linija.draw()
        linija = Linija(self.boja, self.tocka3, self.tocka1)
        linija.draw()

class Elipsa(Kruznica):
    radius2 = 1.0
    def __init__(self, boja, tocka1, radius, radius2) -> None:
        super().__init__(boja, tocka1, radius)
        self.radius2 = radius2
    def draw(self) -> None:
        canvas.create_oval(self.tocka1[0]-self.radius, self.tocka1[1]-self.radius2, self.tocka1[0]+self.radius, self.tocka1[1]+self.radius2, outline=self.boja)

def drawExamples():
    linija = Linija("#FF0000", [0.0, 0.0], [100.0, 100.0])
    linija.draw()
    prav = Pravokutnik("#00FF00", [100.0, 100.0], 200.0, 300.0)
    prav.draw()
    poly = Poligon("#0000FF", [200.0, 200.0], [300.0, 300.0, 400.0, 200.0, 200.0, 100.0])
    poly.draw()
    krug = Kruznica("#0f0f0f", [50.0, 50.0], 50)
    krug.draw()
    trokut = Trokut("#f0f0f0", [400.0, 400.0], [500.0, 500.0], [450.0, 100.0])
    trokut.draw()
    elipsa = Elipsa("#0c0c0c", [200.0, 200.0], 100.0, 50.0)
    elipsa.draw()


def openFile():
    fileName = filedialog.askopenfile(mode='r', defaultextension='.txt').name
    fp = open(fileName, "r")
    numObjects = fp.readlines().__len__()
    fp = open(fileName, "r")
    for i in range(0, numObjects):
        line = fp.readline()
        grafObject = line.split()
        if grafObject[0] == "Line":
            line = Linija(grafObject[1], [float(grafObject[2]), float(grafObject[3])], [float(grafObject[4]), float(grafObject[5])])
            line.draw()
        elif grafObject[0] == "Triangle":
            triangle = Trokut(grafObject[1], [float(grafObject[2]), float(grafObject[3])], [float(grafObject[4]), float(grafObject[5])], [float(grafObject[6]), float(grafObject[7])])
            triangle.draw()
        elif grafObject[0] == "Rectangle":
            rectangle = Pravokutnik(grafObject[1], [float(grafObject[2]), float(grafObject[3])], float(grafObject[4]), float(grafObject[5]))
            rectangle.draw()
        elif grafObject[0] == "Circle":
            circle = Kruznica(grafObject[1], [float(grafObject[2]), float(grafObject[3])], float(grafObject[4]))
            circle.draw()
        elif grafObject[0] == "Ellipse":
            ellipse = Elipsa(grafObject[1], [float(grafObject[2]), float(grafObject[3])], float(grafObject[4]), float(grafObject[5]))
            ellipse.draw()
        elif grafObject[0] == "Polygon":
            pointsStr = grafObject[4:grafObject.__len__()]
            points =[float(point) for point in pointsStr]
            print(points)
            polygon = Poligon(grafObject[1], [float(grafObject[2]), float(grafObject[3])], points)
            polygon.draw()

def Srv_func(cs): #Funkcija koja će se izvršavati za svakog klijenta nezavisno u zasebnoj niti
    while True:
        message = cs.recv(1024).decode() #Dohvaća poslanu poruku i dekodira u string
        print(message)
        grafObject = message.split()
        if grafObject[0] == "Line":
            line = Linija(grafObject[1], [float(grafObject[2]), float(grafObject[3])], [float(grafObject[4]), float(grafObject[5])])
            line.draw()
        elif grafObject[0] == "Triangle":
            triangle = Trokut(grafObject[1], [float(grafObject[2]), float(grafObject[3])], [float(grafObject[4]), float(grafObject[5])], [float(grafObject[6]), float(grafObject[7])])
            triangle.draw()
        elif grafObject[0] == "Rectangle":
            rectangle = Pravokutnik(grafObject[1], [float(grafObject[2]), float(grafObject[3])], float(grafObject[4]), float(grafObject[5]))
            rectangle.draw()
        elif grafObject[0] == "Circle":
            circle = Kruznica(grafObject[1], [float(grafObject[2]), float(grafObject[3])], float(grafObject[4]))
            circle.draw()
        elif grafObject[0] == "Ellipse":
            ellipse = Elipsa(grafObject[1], [float(grafObject[2]), float(grafObject[3])], float(grafObject[4]), float(grafObject[5]))
            ellipse.draw()
        elif grafObject[0] == "Polygon":
            pointsStr = grafObject[4:grafObject.__len__()]
            points =[float(point) for point in pointsStr]
            print(points)
            polygon = Poligon(grafObject[1], [float(grafObject[2]), float(grafObject[3])], points)
            polygon.draw()

def call_func():
    listensocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
    port = 8000
    maxConnections = 10
    name = socket.gethostname() 
    listensocket.bind(("localhost" ,port)) 
    listensocket.listen(maxConnections)
    serverMessage = "Started server at " + name + " on port " + str(port)
    print(serverMessage)
    canvas.create_text(880, 690, text=serverMessage, fill="black", font=('Helvetica 10'))
    canvas.pack()
    while True:
        (clientsocket, address) = listensocket.accept() 
        print("New connection made from address: ", address)
        t = thr.Thread(target=Srv_func, args=(clientsocket,)) 
        t.daemon = True
        t.start()

def startServer():
    t1 = thr.Thread(target=call_func) 
    t1.daemon = True
    t1.start()



def closeFile():
    canvas.delete("all")

menubar = tk.Menu(root)
root.config(menu=menubar)
fileMenu = tk.Menu(menubar, tearoff=False)
fileMenu.add_command(command=openFile, label="Open")
fileMenu.add_command(command=closeFile, label="Close")
menubar.add_cascade(label="File", menu=fileMenu)
serverMenu = tk.Menu(menubar, tearoff=False)
serverMenu.add_command(command=startServer, label="Start Server")
menubar.add_cascade(label="Server", menu=serverMenu)

#drawExamples()
root.mainloop()

        
        